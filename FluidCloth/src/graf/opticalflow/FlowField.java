/**
 * 
 */
package graf.opticalflow;

/**
 * @author Marcus
 *
 */
public class FlowField {
	
	 /*
	 *
	 * http://petewarden.com/notes/archives/2005/05/gpu_optical_flo.html
	 * 
	 * (1) CurrentDifference = (Frame1[x][y]-Frame0[x][y]);
	 * 
	 * (2) GradientX = ((Frame1[x+1][y]-Frame1[x-1][y]) + (Frame0[x+1][y]-Frame0[x-1][y])) / 2.0;		 * 
	 * (3) GradientY = ((Frame1[x][y+1]-Frame1[x][y-1]) + (Frame0[x][y+1]-Frame0[x][y-1])) / 2.0; 
	 * 
	 * (4) GradientMagnitude = sqrt((GradientX*GradientX)+(GradientY*GradientY)); 
	 * 
	 * (5) VelocityX = CurrentDifference*(GradientX/GradientMagnitude);
	 * (6) VelocityY = CurrentDifference*(GradientY/GradientMagnitude); 
	 * 
	 */
	
	/**
	 * Noise filter sensitivity:
	 * Low values give you a very sensitive but noisy result, 
	 * high values filter out the noise, but miss small objects.
	 * <br>
	 * Typically use values between 0 and 1. Default is 0;
	 */
	public static float lambda = 0;
	
	/**
	 * How strong the blur is 0..1
	 * Blurs velocity values with neighbouring values.
	 * If set to 0, faster performance.
	 * Default is 1;
	 */
	public static float blurFac = 1;
	
	/**
	 * Velocity arrays. Only set after a call to {@link #calculate(int[], int[], int, int)}
	 * Values -1..1
	 */
	public static float[] velocityX, velocityY;
	
	/**
	 * Velocity magnitudes
	 */
	public static float[] velocityMagnitude;
	
	/**
	 * Brightness arrays. Only set after a call to {@link #calculate(int[], int[], int, int)}
	 * values 0..1
	 */
	public static float[] bright0, bright1;
	
	/**
	 *	Dimension of flow field. Only set after a call to {@link #calculate(int[], int[], int, int)} 
	 */
	public static int width, height;
	
	/**
	 * Calculate flow field and store in {@link #velocityX} and {@link #velocityY}
	 * @param frame0 rgb array of last frame
	 * @param frame1 rgb array of current frame
	 * @param frameWidth width of frame
	 * @param frameHeight height of frame
	 */
	public static void calculate(int[] frame0, int[] frame1, int frameWidth, int frameHeight) {
		int len = frameWidth*frameHeight;
		width = frameWidth;
		height = frameHeight;
		if (velocityX==null || velocityX.length!=len) {
			velocityX = new float[len];
			velocityY = new float[len];
			velocityMagnitude = new float[len];
			bright0 = new float[len];
			bright1 = new float[len];
		}
		
		/*
		 * Create brightness array
		 */
		int x, y;
		
		int i = 0;//1+w1;
		for (y=0;y<frameHeight;y++) {
			for (x=0;x<frameWidth;x++) {
				bright0[i] = ((frame0[i]>>16&0xff)+(frame0[i]>>8&0xff)+(frame0[i]&0xff))/765.0f;
				bright1[i] = ((frame1[i]>>16&0xff)+(frame1[i]>>8&0xff)+(frame1[i]&0xff))/765.0f;
				i++;
			}
		}
		
		
		int w1 = frameWidth-1, h1 = frameHeight-1;
		
		float[] cd = new float[len];
		float[] gradX = new float[len];
		float[] gradY = new float[len];
		float[] gradMag = new float[len];
		
		i = 1+w1;
		for (y=1;y<h1;y++) {
			for (x=1;x<w1;x++) {
				// (1) CurrentDifference = (Frame1[x][y]-Frame0[x][y]);
				// cd range: -1..1
				cd[i] = bright1[i]-bright0[i];
				// (2) GradientX = ((Frame1[x+1][y]-Frame1[x-1][y]) + (Frame0[x+1][y]-Frame0[x-1][y])) / 2.0; 
				// (3) GradientY = ((Frame1[x][y+1]-Frame1[x][y-1]) + (Frame0[x][y+1]-Frame0[x][y-1])) / 2.0;
				// grad range: -1..1
				gradX[i] = -((bright1[i+1]-bright1[i-1]) + (bright0[i+1]-bright0[i-1])) / 2;
				gradY[i] = -((bright1[i+frameWidth]-bright1[i-frameWidth]) + (bright0[i+frameWidth]-bright0[i-frameWidth])) / 2;
				// (4) GradientMagnitude = sqrt((GradientX*GradientX)+(GradientY*GradientY));
				gradMag[i] = (float)Math.sqrt(gradX[i]*gradX[i] + gradY[i]*gradY[i] + lambda);
				// (5) VelocityX = CurrentDifference*(GradientX/GradientMagnitude);
				// (6) VelocityY = CurrentDifference*(GradientY/GradientMagnitude);
				velocityX[i] = cd[i]*(gradX[i]/gradMag[i])/765.0f;
				velocityY[i] = cd[i]*(gradY[i]/gradMag[i])/765.0f;
				if (blurFac==0) {
					velocityMagnitude[i] = (float)Math.sqrt(velocityX[i]*velocityX[i]+velocityY[i]*velocityY[i]);
				}
				i++;
			}
			i+=2;
		}
		
		if (blurFac>0) {
		
			float[] velXblur1 = new float[len];
			float[] velYblur1 = new float[len];
			
			float hereFac = 1-blurFac;
			float nFac = blurFac;
			float d2 = hereFac+nFac;
			float d3 = hereFac+2*nFac;
			
			
			/*
			 * Horizontal blur
			 */
			i=0;
			for (y=0;y<height;y++) {
				for (x=0;x<width;x++) {
					if (x==0) {
						velXblur1[i] = (hereFac*velocityX[i]+velocityX[i+1])/d2;
						velYblur1[i] = (hereFac*velocityY[i]+velocityY[i+1])/d2;
					} else if (x==width-1) {
						velXblur1[i] = (nFac*velocityX[i-1]+hereFac*velocityX[i])/d2;
						velYblur1[i] = (nFac*velocityY[i-1]+hereFac*velocityY[i])/d2;
					} else {
						velXblur1[i] = (nFac*velocityX[i-1]+hereFac*velocityX[i]+nFac*velocityX[i+1])/d3;
						velYblur1[i] = (nFac*velocityY[i-1]+hereFac*velocityY[i]+nFac*velocityY[i+1])/d3;
					}
					i++;
				}
			}
			/*
			 * Vertical blur
			 */
			i=0;
			for (x=0;x<width;x++) {
				i=x;
				for (y=0;y<height;y++) {
					if (y==0) {
						velocityX[i] = (hereFac*velXblur1[i]+nFac*velXblur1[i+width])/d2;
						velocityY[i] = (hereFac*velYblur1[i]+nFac*velYblur1[i+width])/d2;
					} else if (y==height-1) {
						velocityX[i] = (nFac*velXblur1[i-width]+hereFac*velXblur1[i])/d2;
						velocityY[i] = (nFac*velYblur1[i-width]+hereFac*velYblur1[i])/d2;
					} else {
						velocityX[i] = (nFac*velXblur1[i-width]+hereFac*velXblur1[i]+nFac*velXblur1[i+width])/d3;
						velocityY[i] = (nFac*velYblur1[i-width]+hereFac*velYblur1[i]+nFac*velYblur1[i+width])/d3;
					}
					velocityMagnitude[i] = (float)Math.sqrt(velocityX[i]*velocityX[i]+velocityY[i]*velocityY[i]);
					i+=width;
				}
			}
			
		}
	}

	/**
	 * Thresholds the velocityMagnitude values. 
	 * Any velocityMagnitude below the supplied value will be set to 0.
	 * Affects {@link #velocityX}, {@link #velocityY} and {@link #velocityMagnitude}.
	 * <p>
	 * Typical value would be something like 0.00005f;
	 * @param f
	 */
	public static void thresh(float f) {
		int i;
		//int x, y;
		int len = velocityX.length;
		//for (y=0;y<height;y++) {
			//for (x=0;x<width;x++) {
		for (i=0;i<len;i++) {
				if (velocityMagnitude[i]<f) {
					velocityX[i]=0;
					velocityY[i]=0;
					velocityMagnitude[i] = 0;
				}
		}
				//i++;
			//}
		//}
	}

	/**
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public static void deleteRect(int x, int y, int rectWidth, int recHeight) {
		int i = y*width+x;
		for (int yy=0;yy<recHeight;yy++) {
			for (int xx=0;xx<rectWidth;xx++) {
				velocityX[i] = 0;
				velocityY[i] = 0;
				velocityMagnitude[i] = 0;
				i++;
			}
			i-=rectWidth;
			i+=width;
		}
	}
	
}
