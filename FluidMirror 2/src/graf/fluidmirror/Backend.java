/**
 * 
 */
package graf.fluidmirror;

import graf.gamefluids.FluidRectRGB;
import graf.opticalflow.FlowFieldObject;

import java.awt.Point;

import processing.core.PApplet;

/**
 * @author Marcus
 *
 */
public class Backend {
	
	private final PApplet pa;
	
	private final Webcam webcam;
	private final Point webcamPos;
	
	private final FlowFieldObject flowField;
	private final Point flowFieldPos;
	private final float flowFieldVectorScale = 50000;
	
	private final FlowFieldObject loResFlowField;
	private final Point lffPos;
	private final float lffScale = 4;
	private final float lffVelScale = 200;
	
	private final FluidRectRGB fluid;
	private final Point fluidPos;
	private final float fluidScale = 4;
	
	public Backend(
			PApplet pa, 
			Webcam webcam, 
			FlowFieldObject flowField, 
			FlowFieldObject loResFlowField,
			FluidRectRGB fluid) {
		
		this.pa = pa;
		
		this.webcam = webcam;	
		webcamPos = new Point(10,10);
		
		this.flowField = flowField;
		flowFieldPos = new Point(20+webcam.width,10);
		
		this.loResFlowField = loResFlowField;
		lffPos = new Point(10,20+webcam.height);
		
		this.fluid = fluid;
		fluidPos = new Point((int)(20+loResFlowField.width*lffScale),(int)(20+webcam.height));
	}


	public void drawWebcam() {
		pa.fill(0);
		pa.image(webcam.getPImage(),webcamPos.x,webcamPos.y);
	}

	
	
	public void drawFlowField() {
		pa.stroke(255);
		pa.noFill();
		pa.rectMode(PApplet.CORNER);
		pa.rect(flowFieldPos.x,flowFieldPos.y,webcam.width,webcam.height);
		
		pa.stroke(255,64,64);
		int i=0;
		float x0,y0,x1,y1;
		float[] vx, vy;
		vx = flowField.getVelocityX();
		vy = flowField.getVelocityY();
		for (int y=0;y<webcam.height;y++) {
			for (int x=0;x<webcam.width;x++) {
				if (vx[i]!=0 || vy[i]!=0) {
					x0 = flowFieldPos.x + x;//*ffdisplayScale;
					x1 = x0 + flowFieldVectorScale * vx[i];
					y0 = flowFieldPos.y + y;//*ffdisplayScale;
					y1 = y0 + flowFieldVectorScale * vy[i];
					pa.line(x0,y0,x1,y1);
				}
				i++;
			}
		}
	}
	
	
	
	/**
	 * @param mouseX
	 * @param mouseY
	 */
	public void mousePressed(int mouseX, int mouseY) {
		if ( mouseX>=webcamPos.x && mouseX<webcamPos.x+webcam.width
				&& mouseY>=webcamPos.y && mouseY<webcamPos.y+webcam.height ) {
//			webcam.settings();
		}
	}


	/**
	 * 
	 */
	public void drawLoResFlowField() {
		pa.stroke(255);
		pa.noFill();
		pa.rectMode(PApplet.CORNER);
		pa.rect(lffPos.x,lffPos.y,loResFlowField.width*lffScale,loResFlowField.height*lffScale);
		
		pa.stroke(255,64,64);
		int i=0;
		float x0,y0,x1,y1;
		float[] vx = loResFlowField.getVelocityX();
		float[] vy = loResFlowField.getVelocityY();
		for (int y=0;y<loResFlowField.height;y++) {
			for (int x=0;x<loResFlowField.width;x++) {
				if (vx[i]!=0 || vy[i]!=0) {
					x0 = lffPos.x + x*lffScale;
					x1 = x0 + lffVelScale * vx[i];
					y0 = lffPos.y + y*lffScale;
					y1 = y0 + lffVelScale * vy[i];
					pa.line(x0,y0,x1,y1);
				}
				i++;
			}
		}
	}


	/**
	 * 
	 */
	public void drawFluid() {
		float[] densityR = fluid.getDensityR();
		float[] densityG = fluid.getDensityG();
		float[] densityB = fluid.getDensityB();
		
		pa.noStroke();
		pa.rectMode(PApplet.CORNER);
		int i=0, x, y;
		int r, g, b;
		for (y=0;y<fluid.HEIGHT;y++) {
			for (x=0;x<fluid.WIDTH;x++) {
				r = (int)Math.min(255,densityR[i]);
				g = (int)Math.min(255,densityG[i]);
				b = (int)Math.min(255,densityB[i]);
				if (r>0||g>0||b>0) {
					pa.fill(r,g,b,255);
					pa.rect(fluidPos.x+x*fluidScale,fluidPos.y+y*fluidScale,fluidScale,fluidScale);
				}
				i++;
			}
		}
		
		pa.stroke(255);
		pa.noFill();
		pa.rect(fluidPos.x,fluidPos.y, fluid.WIDTH*fluidScale, fluid.HEIGHT*fluidScale);
		
	}
	
	
}
