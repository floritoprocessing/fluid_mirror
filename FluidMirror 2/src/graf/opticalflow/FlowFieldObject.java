/**
 * 
 */
package graf.opticalflow;

/**
 * @author Marcus
 *
 */
public class FlowFieldObject {
	
	public final int width, height, length;
	
	private float lambda = 0;
	private float blurFac = 1;
	private float velocityThreshold = 0.0001f;
	
	private int pointerToCurrentFrame = 0;
	//private int pointerToLastFrame = 1;
	private int[] frame0;
	private int[] frame1;
	
	
	/**
	 * Velocity arrays. Only set after a call to {@link #calculate(int[], int[], int, int)}
	 * Values -1..1
	 */
	private float[] velocityX, velocityY;
	
	/**
	 * Velocity magnitudes
	 */
	private float[] velocityMagnitude;
	
	/**
	 * Brightness arrays. Only set after a call to {@link #calculate(int[], int[], int, int)}
	 * values 0..1
	 */
	private float[] bright0, bright1;
	
	private float[] cd;
	private float[] gradX;
	private float[] gradY;
	private float[] gradMag;
	private float[] velXblur1;
	private float[] velYblur1;
	
	public FlowFieldObject(int width, int height) {
		this.width = width;
		this.height = height;
		this.length = width*height;
		
		frame0 = new int[length];
		frame1 = new int[length];
		velocityX = new float[length];
		velocityY = new float[length];
		velocityMagnitude = new float[length];
		bright0 = new float[length];
		bright1 = new float[length];
		
		cd = new float[length];
		gradX = new float[length];
		gradY = new float[length];
		gradMag = new float[length];
		velXblur1 = new float[length];
		velYblur1 = new float[length];
	}
	
	public int[] getCurrentFrame() {
		if (pointerToCurrentFrame==0) {
			return frame0;
		} else {
			return frame1;
		}
	}
	
	
	/**
	 * Calculates a new flowField
	 * <ul>
	 * <li> Calculate brightness values
	 * <li> Calculate velocities
	 * <li> Blur velocities (if blur factor is > 0)
	 * <li> Threshold velocities
	 * </ul>
	 * @param newFrame
	 */
	public void calculate(int[] newFrame) {
		
		/*
		 * Swap frames
		 */
		if (pointerToCurrentFrame==0) {
			pointerToCurrentFrame = 1;
			//pointerToLastFrame = 0;
		}
		else {
			pointerToCurrentFrame = 0;
			//pointerToLastFrame = 1;
		}
		
		/*
		 * Update current frame
		 */
		if (pointerToCurrentFrame==0) {
			frame0 = newFrame;
		} else {
			frame1 = newFrame;
		}
		
		/*
		 * Create brightness array of current frame
		 */
		int i;
		if (pointerToCurrentFrame==0) {
			for (i=0;i<length;i++) {
				bright0[i] = ((frame0[i]>>16&0xff)+(frame0[i]>>8&0xff)+(frame0[i]&0xff))/765.0f;
			}
		} else {
			for (i=0;i<length;i++) {
				bright1[i] = ((frame1[i]>>16&0xff)+(frame1[i]>>8&0xff)+(frame1[i]&0xff))/765.0f;
			}
		}
		
		/*
		 * Calculate velocities
		 */
		int w1 = width-1, h1 = height-1;		
		i = 1+w1;
		int x, y;
		int currentFrameInverse = pointerToCurrentFrame==0 ? 1 : -1;
		for (y=1;y<h1;y++) {
			for (x=1;x<w1;x++) {
				cd[i] = currentFrameInverse * (bright1[i]-bright0[i]);
				gradX[i] = -((bright1[i+1]-bright1[i-1]) + (bright0[i+1]-bright0[i-1])) / 2;
				gradY[i] = -((bright1[i+width]-bright1[i-width]) + (bright0[i+width]-bright0[i-width])) / 2;
				gradMag[i] = (float)Math.sqrt(gradX[i]*gradX[i] + gradY[i]*gradY[i] + lambda);
				velocityX[i] = cd[i]*(gradX[i]/gradMag[i])/765.0f;
				velocityY[i] = cd[i]*(gradY[i]/gradMag[i])/765.0f;
				if (blurFac==0) {
					velocityMagnitude[i] = (float)Math.sqrt(velocityX[i]*velocityX[i]+velocityY[i]*velocityY[i]);
					if (velocityMagnitude[i]<velocityThreshold) {
						velocityMagnitude[i] = 0;
						velocityX[i] = 0;
						velocityY[i] = 0;
					}
				}
				i++;
			}
			i+=2;
		}
		
		
		/*
		 * Blur velocities
		 */
		if (blurFac>0) {
		
			float hereFac = 1-blurFac;
			float nFac = blurFac;
			float d2 = hereFac+nFac;
			float d3 = hereFac+2*nFac;
			
			
			/*
			 * Horizontal blur
			 */
			
			i=0;
			for (y=0;y<height;y++) {
				for (x=0;x<width;x++) {
					if (x==0) {
						velXblur1[i] = (hereFac*velocityX[i]+velocityX[i+1])/d2;
						velYblur1[i] = (hereFac*velocityY[i]+velocityY[i+1])/d2;
					} else if (x==width-1) {
						velXblur1[i] = (nFac*velocityX[i-1]+hereFac*velocityX[i])/d2;
						velYblur1[i] = (nFac*velocityY[i-1]+hereFac*velocityY[i])/d2;
					} else {
						velXblur1[i] = (nFac*velocityX[i-1]+hereFac*velocityX[i]+nFac*velocityX[i+1])/d3;
						velYblur1[i] = (nFac*velocityY[i-1]+hereFac*velocityY[i]+nFac*velocityY[i+1])/d3;
					}
					i++;
				}
			}
			
			/*
			 * Vertical blur
			 */
			i=0;
			for (x=0;x<width;x++) {
				i=x;
				for (y=0;y<height;y++) {
					if (y==0) {
						velocityX[i] = (hereFac*velXblur1[i]+nFac*velXblur1[i+width])/d2;
						velocityY[i] = (hereFac*velYblur1[i]+nFac*velYblur1[i+width])/d2;
					} else if (y==height-1) {
						velocityX[i] = (nFac*velXblur1[i-width]+hereFac*velXblur1[i])/d2;
						velocityY[i] = (nFac*velYblur1[i-width]+hereFac*velYblur1[i])/d2;
					} else {
						velocityX[i] = (nFac*velXblur1[i-width]+hereFac*velXblur1[i]+nFac*velXblur1[i+width])/d3;
						velocityY[i] = (nFac*velYblur1[i-width]+hereFac*velYblur1[i]+nFac*velYblur1[i+width])/d3;
					}
					velocityMagnitude[i] = (float)Math.sqrt(velocityX[i]*velocityX[i]+velocityY[i]*velocityY[i]);
					if (velocityMagnitude[i]<velocityThreshold) {
						velocityMagnitude[i] = 0;
						velocityX[i] = 0;
						velocityY[i] = 0;
					}
					i+=width;
				}
			}
			
		}
		
		
		
		
		
	}


	/**
	 * Returns the blur factor
	 * @return blur factor
	 */
	public float getBlurFac() {
		return blurFac;
	}

	
	/**
	 * Returns the noise filter sensitivity
	 * @return
	 */
	public float getLambda() {
		return lambda;
	}


	/**
	 * Returns the velocity threshold
	 * @return
	 */
	public float getVelocityThreshold() {
		return velocityThreshold;
	}
	
	
	/**
	 * Sets the blur factor. The blur factor averages the velocity vectors.
	 * If set to 0, higher performance. Default value is 1.
	 * @param blurFac
	 */
	public void setBlurFac(float blurFac) {
		this.blurFac = blurFac;
	}

	/**
	 * Sets the noise filter sensitivity:
	 * <br>
	 * Low values give you a very sensitive but noisy result,
	 * high values filter out the noise, but miss small objects.
	 * <br>
	 * Typically use values between 0 and 1. Default is 0;
	 * @param lambda
	 */
	public void setLambda(float lambda) {
		this.lambda = lambda;
	}

	/**
	 * Sets the velocity threshold.
	 * If velocity magnitudes are below this value, they are set to 0.
	 * Default value is 0.0001f
	 * @param velocityThreshold
	 */
	public void setVelocityThreshold(float velocityThreshold) {
		this.velocityThreshold = velocityThreshold;
	}


	public float[] getVelocityX() {
		return velocityX;
	}
	
	public float[] getClonedVelocityX() {
		float[] out = new float[velocityX.length];
		System.arraycopy(velocityX, 0, out, 0, velocityX.length);
		return out;
	}


	public float[] getVelocityY() {
		return velocityY;
	}
	
	public float[] getClonedVelocityY() {
		float[] out = new float[velocityY.length];
		System.arraycopy(velocityY, 0, out, 0, velocityY.length);
		return out;
	}


	public float[] getVelocityMagnitude() {
		return velocityMagnitude;
	}


	/**
	 * Creates scaled copy with {@link #velocityX}, {@link #velocityY}, {@link #velocityMagnitude}
	 * and {@link #frame0}
	 * @param src
	 * @param dst
	 * @param velocityScale
	 */
	public static void rescale(FlowFieldObject src, FlowFieldObject dst, float velocityScale) {
		float divx = (float)src.width / dst.width;
		float divy = (float)src.height / dst.height;
		dst.frame0 = new int[dst.width*dst.height];
		
		
		int dsti=0, dstx, dsty;
		float vx, vy, vmag;
		int si, sx, sy, sx0, sx1, sy0, sy1, scount;
		for (dsty=0;dsty<dst.height;dsty++) {
			sy0 = (int)(dsty*divy);
			sy1 = (int)((dsty+1)*divy);
			for (dstx=0;dstx<dst.width;dstx++) {
				sx0 = (int)(dstx*divx);
				sx1 = (int)((dstx+1)*divx);
				scount = 0;
				
				vx=0;
				vy=0;
				vmag=0;
				for (sy=sy0;sy<sy1;sy++) {
					si = sy*src.width + sx0;
					if (sy==sy0) {
						dst.frame0[dsti] = src.getCurrentFrame()[si];
					}
					for (sx=sx0;sx<sx1;sx++) {
						vx += src.velocityX[si];
						vy += src.velocityY[si];
						vmag += src.velocityMagnitude[si];
						scount++;
						si++;
					}
				}
				vx /= scount;
				vy /= scount;
				vmag /= scount;
				
				
				dst.velocityX[dsti] = vx * velocityScale;
				dst.velocityY[dsti] = vy * velocityScale;
				dst.velocityMagnitude[dsti] = vmag * Math.abs(velocityScale);
				dsti++;
				
			}
		}
		
	}
	
}
