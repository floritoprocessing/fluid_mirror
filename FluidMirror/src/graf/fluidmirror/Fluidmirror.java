/**
 * 
 */
package graf.fluidmirror;

import java.nio.ByteBuffer;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.glu.GLU;

import graf.gamefluids.FluidRectRGB;
import graf.opticalflow.FlowFieldObject;
import processing.core.PApplet;
import processing.core.PImage;

/**
 * @author Marcus
 *
 */
public class Fluidmirror extends PApplet {
	
	private static final long serialVersionUID = -8795408503156675645L;

	public static boolean DEBUG = true;

	public static void main(String[] args) {
		//PApplet.main(new String[] {"graf.fluidmirror.Fluidmirror"});
		PApplet.main(new String[] {"--present","graf.fluidmirror.Fluidmirror"});
	}
	
	private Webcam webcam;
	private FlowFieldObject flowField, loResFlowField;
	private float[] densitiesR, densitiesG, densitiesB;
	
	private FluidRectRGB fluidRGB;
	private final float fluidTopClip = 255;
	
	private final int WEBCAM_WIDTH = 320;
	private final int WEBCAM_HEIGHT = 240;
	
	private final int FLUID_WIDTH = 128; //120x90
	private final int FLUID_HEIGHT = 96;
	
	private final float loResFlowAmplification = -255;
	
	//private float dt = 1.5f; // 1.0f
	//private float diffusion = 0.00001f;
	//private float viscosity = 0.00001f;
	//private float evaporation = 0.8f;
	
	private boolean backendActive = false;
	private Backend backend;
	
	private Preset preset = new PresetGentle();
	
	public void draw() {
		if (frameCount<100) {
			background(0);
			float x0 = 20;
			float x1 = width-20;
			float y0 = height/2-10;
			float y1 = height/2+10;
			
			noStroke();
			fill(255,0,0);
			rectMode(CORNERS);
			rect(x0,y0,x0 + ((float)frameCount/100)*(x1-x0),y1);
			
			stroke(255);
			noFill();
			rectMode(CORNERS);
			rect(x0,y0,x1,y1);
			
			fill(0);
			if (frameCount%10==0) webcam.update();
			image(webcam.getPImage(),(width-webcam.width)/2.0f,0.25f*height-0.5f*webcam.height);
			
			//println(frameCount);
			return;
		}
		else if (frameCount==100) {
			if (!backendActive) {
				noCursor();
			}
		}
		
		/*
		 * Update webcam
		 */
		webcam.update();
		
		if (backendActive) {
			background(0);
			backend.drawWebcam();
		}
		
		
		/*
		 * Update flowField
		 */
		flowField.calculate(webcam.getCurrentFrame());
		
		/*
		 * Show FlowField
		 */
		if (backendActive) {
			backend.drawFlowField();
		}
		
		
		/*
		 * Update loResFlowField
		 */
		FlowFieldObject.rescale(
				flowField,
				loResFlowField,
				loResFlowAmplification);
		
		/*
		 * Show loResFlowField
		 */
		if (backendActive) {
			backend.drawLoResFlowField();
		}
		
		
		
		/*
		 * Create input stream
		 */
		
		float[] velX = loResFlowField.getClonedVelocityX();
		float[] velY = loResFlowField.getClonedVelocityY();
		int[] rgbVals = loResFlowField.getCurrentFrame();
		
		
		int i=0, x, y;
		float mag;
		boolean[] filled = new boolean[densitiesR.length];
		
		for (y=0;y<loResFlowField.height;y++) {
			for (x=0;x<loResFlowField.width;x++) {
				mag = loResFlowField.getVelocityMagnitude()[i];
				if (mag>0) {
					densitiesR[i] = 10*mag*(rgbVals[i]>>16&0xff);
					densitiesG[i] = 10*mag*(rgbVals[i]>>8&0xff);
					densitiesB[i] = 10*mag*(rgbVals[i]&0xff);
					//velY[i] -= 0.0003f;
					filled[i] = true;
				} else {
					densitiesR[i] = 0;
					densitiesG[i] = 0;
					densitiesB[i] = 0;
					filled[i] = false;
				}
				i++;
			}
		}
		
		
		boolean extraThick = false;
		if (extraThick) {
			float[] xtraR = new float[densitiesR.length];
			float[] xtraG = new float[densitiesR.length];
			float[] xtraB = new float[densitiesR.length];
			boolean[] xtra = new boolean[densitiesR.length];
			i=0;
			int count=0;
			int row = loResFlowField.width;
			for (y=0;y<loResFlowField.height;y++) {
				for (x=0;x<loResFlowField.width;x++) {
					count=0;
					if (!filled[i]) {
						
						if (x>0 && filled[i-1]) {
							xtraR[i] += densitiesR[i-1];
							xtraG[i] += densitiesG[i-1];
							xtraB[i] += densitiesB[i-1];
							count++;
							xtra[i] = true;
						}
						if (x<loResFlowField.width-1 && filled[i+1]) {
							xtraR[i] += densitiesR[i+1];
							xtraG[i] += densitiesG[i+1];
							xtraB[i] += densitiesB[i+1];
							count++;
							xtra[i] = true;
						}
						if (y>0 && filled[i-row]) {
							xtraR[i] += densitiesR[i-row];
							xtraG[i] += densitiesG[i-row];
							xtraB[i] += densitiesB[i-row];
							count++;
							xtra[i] = true;
						}
						if (y<loResFlowField.height-1 && filled[i+row]) {
							xtraR[i] += densitiesR[i+row];
							xtraG[i] += densitiesG[i+row];
							xtraB[i] += densitiesB[i+row];
							count++;
							xtra[i] = true;
						}
						if (count!=0) {
							count += count;
							xtraR[i] /= count;
							xtraG[i] /= count;
							xtraB[i] /= count;
						}
					}
					
					i++;
				}
			}
			
			i=0;
			for (y=0;y<loResFlowField.height;y++) {
				for (x=0;x<loResFlowField.width;x++) {
					
					if (xtra[i] && !filled[i]) {
						densitiesR[i] = xtraR[i];
						densitiesG[i] = xtraG[i];
						densitiesB[i] = xtraB[i];
					}
					
					
					i++;
				}
			}
			
		}
		// end xtraThick
		
		/*
		for (i=0;i<velY.length;i++) {
			velX[i] += 0.001f;
		}
		*/
		
		fluidRGB.step(
				densitiesR, densitiesG, densitiesB, 
				velX, velY,  
				preset.diffusion, 
				preset.viscosity, 
				preset.dt,
				fluidTopClip);
		fluidRGB.evaporate(
				preset.evaporation, 
				preset.dt);
		
		
		if (backendActive) {
			backend.drawFluid();
		}
		
		
		
		
		
		
		if (!backendActive) {
			//background(255,0,0);
			float[] densityR = fluidRGB.getDensityR();
			float[] densityG = fluidRGB.getDensityG();
			float[] densityB = fluidRGB.getDensityB();
			
			PImage fluidImage = new PImage(fluidRGB.WIDTH,fluidRGB.HEIGHT);
			i=0;
			int rr, gg, bb;
			for (y=0;y<fluidRGB.HEIGHT;y++) {
				for (x=0;x<fluidRGB.WIDTH;x++) {
					rr = (int)min(255,densityR[i]);
					gg = (int)min(255,densityG[i]);
					bb = (int)min(255,densityB[i]);
					fluidImage.pixels[i] = color(rr,gg,bb);
					i++;
				}
			}
			fluidImage.updatePixels();

			
			boolean openglStyle = false;
			
			/*
			if (openglStyle) {
				PGraphicsOpenGL pgl = (PGraphicsOpenGL) g;
				GLU glu = pgl.glu;
				GL gl = pgl.beginGL();
				gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
				gl.glClear(GL.GL_COLOR_BUFFER_BIT);// | GL.GL_DEPTH_BUFFER_BIT);
				gl.glEnable(GL.GL_TEXTURE_2D);
				int texture = genTexture(gl);
				println(texture);
				//TODO: MAKE THIS ONLY ONCE!
				gl.glBindTexture(GL.GL_TEXTURE_2D, texture);
				
				
				ByteBuffer pixelBuffer = BufferUtil.newByteBuffer(fluidImage.pixels.length * 3);
				for (i=0;i<fluidImage.pixels.length;i++) {
					//println(fluidImage.pixels[i]);
					pixelBuffer.put((byte)(fluidImage.pixels[i]>>16&0xff));
					pixelBuffer.put((byte)(fluidImage.pixels[i]>>8&0xff));
					pixelBuffer.put((byte)(fluidImage.pixels[i]&0xff));
					//pixelBuffer.put((byte)(0xff));
				}
				pixelBuffer.flip();
				makeRGBTexture(gl, glu, pixelBuffer, fluidImage.width, fluidImage.height, GL.GL_TEXTURE_2D);
				
				gl.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
				gl.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
				
				//TODO: check jog texture
				gl.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_R, GL.GL_CLAMP); // GL_REPEAT
				gl.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP); // GL_REPEAT
				gl.glBindTexture(GL.GL_TEXTURE_2D, texture);
				gl.glBegin(GL.GL_QUADS);
				gl.glTexCoord2f(0, 0);
				gl.glVertex2f(0, 0);
				gl.glTexCoord2f(1, 0);
				gl.glVertex2f(width, 0);
				gl.glTexCoord2f(1, 1);
				gl.glVertex2f(width, height);
				gl.glTexCoord2f(0, 1);
				gl.glVertex2f(0, height);
				gl.glEnd();
				
				pgl.endGL();
			
			}*/
			
			
			//else {
			fill(0);
			smooth();
			image(fluidImage,0,0,width,height);
			
			/*
			fluidImage.save("" +
					"D:\\Processing" +
					"\\_Eclipse" +
					"\\FluidMirror" +
					"\\output" +
					"\\FluidMirrorDemo_"+nf(frameCount-100,4)+".bmp");
			*/
		}
		
		
		//saveFrame("D:\\Processing\\_Eclipse\\FluidMirror\\output\\FluidMirrorDemo_####.bmp")
	}

	/**
	 * @param gl
	 * @param glu
	 * @param texture
	 * @param gl_texture_2d
	 */
	private void makeRGBTexture(GL gl, GLU glu, ByteBuffer img, int width, int height, int target) {
		gl.glTexImage2D(target, 0, GL.GL_RGB, width, height, 0, GL.GL_RGB, GL.GL_UNSIGNED_BYTE, img);
	}

	private int genTexture(GL gl) {
		final int[] tmp = new int[1];
		gl.glGenTextures(1, tmp, 0);
		return tmp[0];
	}
	
	
	
	public void keyPressed() {
		if (key==' ') {
			backendActive = !backendActive;
			background(0);
			if (backendActive) {
				cursor();
			} else {
				noCursor();
			}
		}
	}
	
	
//	public void mousePressed() {
//		if (backendActive) {
//			backend.mousePressed(mouseX,mouseY);
//		}
//	}
	
	public void settings() {
		size(1024,768,P3D);
	}
	public void setup() {
		
		
		webcam = new Webcam(this, WEBCAM_WIDTH, WEBCAM_HEIGHT);
		webcam.update();
		
		flowField = new FlowFieldObject(WEBCAM_WIDTH, WEBCAM_HEIGHT);
		flowField.setBlurFac(0);
		flowField.setLambda(0.5f); //0.5f
		flowField.setVelocityThreshold(0.000015f);//0.00005f
		
		loResFlowField = new FlowFieldObject(FLUID_WIDTH,FLUID_HEIGHT);
		
		//fluid = new FluidRect(FLUID_WIDTH, FLUID_HEIGHT);
		fluidRGB = new FluidRectRGB(FLUID_WIDTH, FLUID_HEIGHT);
		
		densitiesR = new float[fluidRGB.size];
		densitiesG = new float[fluidRGB.size];
		densitiesB = new float[fluidRGB.size];
		
		backend = new Backend(this, webcam, flowField, loResFlowField, fluidRGB);
		
		
	}
}
