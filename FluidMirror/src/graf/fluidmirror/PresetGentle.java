/**
 * 
 */
package graf.fluidmirror;

/**
 * @author Marcus
 *
 */
public class PresetGentle extends Preset {
	
	public float dt = 1.5f; // 1.0f
	public float diffusion = 0.00001f;
	public float viscosity = 0.00001f;
	public float evaporation = 0.8f;
	
	public PresetGentle() {
	}
}
