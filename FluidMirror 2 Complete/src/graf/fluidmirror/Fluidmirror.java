/**
 * 
 */
package graf.fluidmirror;

import graf.fluidmirror.preset.PresetGentle;
import graf.fluidmirror.preset.PresetGoo;
import graf.fluidmirror.preset.PresetTunnel;
import graf.gamefluids.FluidRectRGB;
import graf.opticalflow.FlowFieldObject;
import processing.core.PApplet;
import processing.core.PImage;

/**
 * @author Marcus
 *
 */
public class Fluidmirror extends PApplet {
	
	private static final long serialVersionUID = -8795408503156675645L;

	public static boolean DEBUG = true;

	public static void main(String[] args) {
		if (args!=null && args.length>2) {
			setSaveIt(new Boolean(args[0]), new Integer(args[1]), args[2]);
		}
		PApplet.main(new String[] {"graf.fluidmirror.Fluidmirror"});
		//PApplet.main(new String[] {"--present","graf.fluidmirror.Fluidmirror"});
	}
	
	public static void setSaveIt(boolean saveImages, int framesToSave, String pathToSaveTo) {
		saveIt = saveImages;
		saveFrameCount = framesToSave;
		saveFramePath = pathToSaveTo;
	}
	private static boolean saveIt = false;
	private static int saveFrameCount = 200;
	private static String saveFramePath = "";
	
	
	private Webcam webcam;
	private FlowFieldObject flowField, loResFlowField;
	private float[] densitiesR, densitiesG, densitiesB;
	
	private FluidRectRGB fluidRGB;
	private final float fluidTopClip = 255;
	
	private final int WEBCAM_WIDTH = 320;
	private final int WEBCAM_HEIGHT = 240;
	
	public static final int FLUID_WIDTH = 80; //128 x 96
	public static final int FLUID_HEIGHT = 60;
	
	//private final float loResFlowVelocityFac = -255;
	
	//private float dt = 1.5f; // 1.0f
	//private float diffusion = 0.00001f;
	//private float viscosity = 0.00001f;
	//private float evaporation = 0.8f;
	
	private boolean backendActive = false;
	private Backend backend;
	
	//private PresetTunnel preset = new PresetTunnel();
	private PresetGentle preset = new PresetGentle();
	//private PresetGoo preset = new PresetGoo();
	
	
	private PImage[] saveImages;// = new PImage[saveFrameCount];
	
	
	
	
	
	public void draw() {
		if (frameCount<100) {
			background(0);
			float x0 = 20;
			float x1 = width-20;
			float y0 = height/2-10;
			float y1 = height/2+10;
			
			noStroke();
			fill(255,0,0);
			rectMode(CORNERS);
			rect(x0,y0,x0 + ((float)frameCount/100)*(x1-x0),y1);
			
			stroke(255);
			noFill();
			rectMode(CORNERS);
			rect(x0,y0,x1,y1);
			
			fill(0);
			if (frameCount%10==0) webcam.update();
			image(webcam.getPImage(),(width-webcam.width)/2.0f,0.25f*height-0.5f*webcam.height);
			
			//println(frameCount);
			return;
		}
		else if (frameCount==100) {
			if (!backendActive) {
				noCursor();
			}
		}
		
		/*
		 * Update webcam
		 */
		webcam.update();
		
		if (backendActive) {
			background(0);
			backend.drawWebcam();
		}
		
		
		/*
		 * Update flowField
		 */
		flowField.calculate(webcam.getCurrentFrame());
		
		/*
		 * Show FlowField
		 */
		if (backendActive) {
			backend.drawFlowField();
		}
		
		
		/*
		 * Update loResFlowField
		 */
		FlowFieldObject.rescale(
				flowField,
				loResFlowField,
				preset.loResFlowVelocityFac);
		
		/*
		 * Show loResFlowField
		 */
		if (backendActive) {
			backend.drawLoResFlowField();
		}
		
		
		
		/*
		 * Create input stream
		 */
		
		float[] velX = loResFlowField.getVelocityX();
		float[] velY = loResFlowField.getVelocityY();
		int[] rgbVals = loResFlowField.getCurrentFrame();
		
		
		int i=0, x, y;
		float mag;
		boolean[] filled = new boolean[densitiesR.length];
		
		for (y=0;y<loResFlowField.height;y++) {
			for (x=0;x<loResFlowField.width;x++) {
				mag = loResFlowField.getVelocityMagnitude()[i];
				if (mag>0) {
					mag *= preset.velocityToDensityFac;
					densitiesR[i] = mag*(rgbVals[i]>>16&0xff);
					densitiesG[i] = mag*(rgbVals[i]>>8&0xff);
					densitiesB[i] = mag*(rgbVals[i]&0xff);
					//velY[i] -= 0.0003f;
					filled[i] = true;
				} else {
					densitiesR[i] = 0;
					densitiesG[i] = 0;
					densitiesB[i] = 0;
					filled[i] = false;
				}
				i++;
			}
		}
		
		
		
		/*
		 * Extra thick
		 */
		
		if (preset.extraThick) {
			float[] xtraR = new float[densitiesR.length];
			float[] xtraG = new float[densitiesR.length];
			float[] xtraB = new float[densitiesR.length];
			boolean[] xtra = new boolean[densitiesR.length];
			i=0;
			int count=0;
			int row = loResFlowField.width;
			for (y=0;y<loResFlowField.height;y++) {
				for (x=0;x<loResFlowField.width;x++) {
					count=0;
					if (!filled[i]) {
						
						if (x>0 && filled[i-1]) {
							xtraR[i] += densitiesR[i-1];
							xtraG[i] += densitiesG[i-1];
							xtraB[i] += densitiesB[i-1];
							count++;
							xtra[i] = true;
						}
						if (x<loResFlowField.width-1 && filled[i+1]) {
							xtraR[i] += densitiesR[i+1];
							xtraG[i] += densitiesG[i+1];
							xtraB[i] += densitiesB[i+1];
							count++;
							xtra[i] = true;
						}
						if (y>0 && filled[i-row]) {
							xtraR[i] += densitiesR[i-row];
							xtraG[i] += densitiesG[i-row];
							xtraB[i] += densitiesB[i-row];
							count++;
							xtra[i] = true;
						}
						if (y<loResFlowField.height-1 && filled[i+row]) {
							xtraR[i] += densitiesR[i+row];
							xtraG[i] += densitiesG[i+row];
							xtraB[i] += densitiesB[i+row];
							count++;
							xtra[i] = true;
						}
						if (count!=0) {
							count += count;
							xtraR[i] /= count;
							xtraG[i] /= count;
							xtraB[i] /= count;
						}
					}
					
					i++;
				}
			}
			
			i=0;
			for (y=0;y<loResFlowField.height;y++) {
				for (x=0;x<loResFlowField.width;x++) {
					if (xtra[i] && !filled[i]) {
						densitiesR[i] = xtraR[i];
						densitiesG[i] = xtraG[i];
						densitiesB[i] = xtraB[i];
					}
					i++;
				}
			}
			
		}
		// end xtraThick
		
		
		/*
		 * Extra velocity
		 */
		if (preset.extraVelocity) {
			if (preset.extraVelocitiesX == null) {
				for (i=0;i<velY.length;i++) {
					velX[i] += preset.extraVelocityX;//0.001f;
					velY[i] += preset.extraVelocityY;
				}
			} else {
				for (i=0;i<velY.length;i++) {
					velX[i] += preset.extraVelocitiesX[i];//0.001f;
					velY[i] += preset.extraVelocitiesY[i];
				}
			}
		}

		
		
		fluidRGB.step(
				densitiesR, densitiesG, densitiesB, 
				velX, velY,  
				preset.diffusion, 
				preset.viscosity, 
				preset.dt,
				fluidTopClip);
		fluidRGB.evaporate(
				preset.evaporation, 
				preset.dt);
		
		
		if (backendActive) {
			backend.drawFluid();
		}
		
		
		
		
		
		
		if (!backendActive) {
			//background(255,0,0);
			float[] densityR = fluidRGB.getDensityR();
			float[] densityG = fluidRGB.getDensityG();
			float[] densityB = fluidRGB.getDensityB();
			
			PImage fluidImage = new PImage(fluidRGB.WIDTH,fluidRGB.HEIGHT);
			i=0;
			int rr, gg, bb;
			for (y=0;y<fluidRGB.HEIGHT;y++) {
				for (x=0;x<fluidRGB.WIDTH;x++) {
					rr = (int)min(255,densityR[i]);
					gg = (int)min(255,densityG[i]);
					bb = (int)min(255,densityB[i]);
					fluidImage.pixels[i] = rr<<16|gg<<8|bb;//blendColor(wcFrame[wi], color(rr,gg,bb,totalDens), BLEND);//)color(rr,gg,bb);
					i++;
				}
			}
			fluidImage.updatePixels();
			
			//TODO: possible overlay function?
			//fluidImage.copy(webcam.getPImage(), 0, 0, WEBCAM_WIDTH, WEBCAM_HEIGHT, 0, 0, fluidImage.width, fluidImage.height);

			
			
			fill(0);
			smooth();
			image(fluidImage,0,0,width,height);
			
		}
		
		//aveFrame("D:\\FluidMirror\\FluidMirror_####.bmp");
		
		if (saveIt) {
			
			int frameToSave = frameCount-100;
			
			if (frameToSave<saveImages.length) {
				saveImages[frameToSave].copy(g, 0, 0, width, height, 0, 0, saveImages[frameToSave].width, saveImages[frameToSave].height);
			} else  {
				saveIt = false;
				println("Saving images");
				for (i=0;i<saveImages.length;i++) {
					println("saving frame "+i);
					saveImages[i].save(saveFramePath+"\\FluidMirror_"+nf(i,4)+".bmp");
				}
			}
			
		}
		
	}	
	
	
	
	public void keyPressed() {
		if (key==' ') {
			backendActive = !backendActive;
			background(0);
			if (backendActive) {
				cursor();
			} else {
				noCursor();
			}
		}
	}
	
	
	public void mousePressed() {
		if (backendActive) {
			backend.mousePressed(mouseX,mouseY);
		}
	}
	
	public void setup() {
		size(1024,768,OPENGL);
		
		if (saveIt) {
			println("Saving the first "+saveFrameCount+" frames");
			saveImages = new PImage[saveFrameCount];
			for (int i=0;i<saveImages.length;i++) {
				saveImages[i] = new PImage(640,480);
			}
		}
		
		webcam = new Webcam(this, WEBCAM_WIDTH, WEBCAM_HEIGHT);
		webcam.update();
		
		flowField = new FlowFieldObject(WEBCAM_WIDTH, WEBCAM_HEIGHT);
		flowField.setBlurFac(0);
		flowField.setLambda(0.5f); //0.5f
		flowField.setVelocityThreshold(0.000015f);//0.00005f
		flowField.setVelocityThreshold(0.0000001f);//0.00005f
		
		loResFlowField = new FlowFieldObject(FLUID_WIDTH,FLUID_HEIGHT);
		
		fluidRGB = new FluidRectRGB(FLUID_WIDTH, FLUID_HEIGHT);
		
		densitiesR = new float[fluidRGB.size];
		densitiesG = new float[fluidRGB.size];
		densitiesB = new float[fluidRGB.size];
		
		backend = new Backend(this, webcam, flowField, loResFlowField, fluidRGB);
		
		
	}
}
