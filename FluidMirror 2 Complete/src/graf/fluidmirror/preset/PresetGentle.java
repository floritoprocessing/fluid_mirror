/**
 * 
 */
package graf.fluidmirror.preset;

import graf.fluidmirror.Fluidmirror;

/**
 * @author Marcus
 *
 */
public class PresetGentle {
	
	public float loResFlowVelocityFac = -255; //-255
	
	public float velocityToDensityFac = 50; //10
	
	public boolean extraThick = false;
	
	public boolean extraVelocity = true;
	public float extraVelocityX = 0;
	public float extraVelocityY = 0;//-0.001f;//0;//-0.001f;
	
	public float[] extraVelocitiesX = null;
	public float[] extraVelocitiesY = null;
	
	public float dt = 1.5f;
	public float diffusion = 0.00001f; //0.00001f
	public float viscosity = 0.00001f;
	public float evaporation = 2.0f; // 0.8
	
}
