/**
 * 
 */
package graf.fluidmirror;

//import JMyron.JMyron;
import processing.core.PApplet;
import processing.core.PImage;
import processing.video.Capture;

/**
 * @author Marcus
 *
 */
public class Webcam {//extends JMyron {
	
	public static boolean DEBUG = true;
	
	private Capture capture;
	
	public final int width;
	public final int height;
		
	private PImage camImage;

	private int pointerAtLastFrame = 0;
	private int pointerAtCurrentFrame = 1;
	private int[] frame0;
	private int[] frame1;

	/**
	 * @param fluidmirror
	 */
	public Webcam(PApplet pa, int requestedWidth, int requestedHeight) {
		super();
		
		width = requestedWidth;
		height = requestedHeight;
		
		PApplet.println(Capture.list());
		capture = new Capture(pa, requestedWidth, requestedHeight);
		
		if (DEBUG) {
			System.out.println("Forced width: "+width);
			System.out.println("Forced height: "+height);
		}
		camImage = new PImage(width,height);
		frame0 = new int[width*height];
		frame1 = new int[width*height];
	}

	/**
	 * The camera image is updated by the {@link #update()} method.
	 * @return the camImage
	 */
	public PImage getPImage() {
		return camImage;
	}

	public void settings() {
		capture.settings();
	}
	
	/**
	 * update JMyron object and camImage
	 */
	public void update() {
		if (capture.available()) {
			capture.read();
			
			//System.out.println(capture.pixels.length);
			//System.out.println(camImage.pixels.length);
			
			System.arraycopy(capture.pixels, 0, camImage.pixels, 0, capture.pixels.length);
			camImage.updatePixels();
			//super.update();
			//cameraImageCopy(camImage.pixels);
			if (pointerAtCurrentFrame==0) {
				pointerAtCurrentFrame=1;
				pointerAtLastFrame=0;
				System.arraycopy(camImage.pixels, 0, frame1, 0, camImage.pixels.length);
			} else {
				pointerAtCurrentFrame=0;
				pointerAtLastFrame=1;
				System.arraycopy(camImage.pixels, 0, frame0, 0, camImage.pixels.length);
			}
			
			//camImage.updatePixels();
		}
		
	}

	/**
	 * Current frame is updated by the {@link #update()} method
	 * @return the current frame
	 */
	public int[] getCurrentFrame() {
		if (pointerAtCurrentFrame==0) {
			return frame0;
		} else {
			return frame1;
		}
	}

	/**
	 * Last frame is updated by the {@link #update()} method
	 * @return the last frame
	 */
	public int[] getLastFrame() {
		if (pointerAtLastFrame==0) {
			return frame0;
		} else {
			return frame1;
		}
	}
	
	

	
}
